const wins = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [6, 4, 2]
];
const nextToWin = [
  // first row
  { indexes: [0, 1], bestMove: 2 },
  { indexes: [1, 2], bestMove: 0 },
  { indexes: [0, 2], bestMove: 1 },
  // second row
  { indexes: [3, 4], bestMove: 5 },
  { indexes: [4, 5], bestMove: 3 },
  { indexes: [3, 5], bestMove: 4 },
  // third row
  { indexes: [6, 7], bestMove: 8 },
  { indexes: [7, 8], bestMove: 6 },
  { indexes: [6, 8], bestMove: 7 },
  // first column
  { indexes: [0, 2], bestMove: 6 },
  { indexes: [3, 6], bestMove: 0 },
  { indexes: [0, 6], bestMove: 3 },
  // second column
  { indexes: [1, 4], bestMove: 7 },
  { indexes: [4, 7], bestMove: 1 },
  { indexes: [1, 7], bestMove: 4 },
  // third column
  { indexes: [2, 5], bestMove: 8 },
  { indexes: [5, 8], bestMove: 2 },
  { indexes: [2, 8], bestMove: 5 },
  // first diagonal
  { indexes: [0, 4], bestMove: 8 },
  { indexes: [4, 8], bestMove: 0 },
  { indexes: [0, 8], bestMove: 4 },
  // second diagonal
  { indexes: [2, 4], bestMove: 6 },
  { indexes: [4, 6], bestMove: 2 },
  { indexes: [2, 6], bestMove: 4 }
];
export function hasWon(board: any[], player: string) {
  let plays = board.reduce((a: any, e: any, i: any) => (e === player ? a.concat(i) : a), []);
  let gameWon = null;
  for (let [index, win] of wins.entries()) {
    if (win.every((elem: any) => plays.indexOf(elem) > -1)) {
      gameWon = { index: index, player: player };
      break;
    }
  }
  return gameWon;
}

function emptySquares(board: any[]) {
  return board.filter(s => typeof s === "number");
}

export function checkTie(board: any[]) {
  if (emptySquares(board).length === 0) {
    return true;
  }
  return false;
}

export function getIndexesOfPlayer(board: any[], player: string): number[] {
  const indexes: number[] = [];
  board
    .map(e => (e === player ? 1 : 0))
    .forEach((n, i) => {
      if (n === 1) {
        indexes.push(i);
      }
    });
  return indexes;
}

export function getRandomMove(board: any[]): number {
  var availSpots = emptySquares(board);
  let place;
  while (true) {
    place = Math.floor(Math.random() * 9);
    if (availSpots.includes(place)) {
      return place;
    }
  }
}

export function getPlayerPossibleWins(board: any[], player: string): any[] {
  const indexes = getIndexesOfPlayer(board, player);
  const possibleWins = [];
  for (let i = 0; i < indexes.length; i++) {
    for (let j = 0; j < indexes.length; j++) {
      const move = nextToWin.find(
        e => JSON.stringify(e.indexes) === JSON.stringify([indexes[i], indexes[j]])
      );
      if (move) {
        possibleWins.push(move);
      }
    }
  }
  return possibleWins;
}

export function move(board: any[], player: string, enemy: string): number {
  const playerPossibleWins = getPlayerPossibleWins(board, player);
  const enemyPossibleWins = getPlayerPossibleWins(board, enemy);

  // enemy got no possible wins make own move
  if (enemyPossibleWins.length === 0) {
    if (playerPossibleWins.length === 0) {
      return getRandomMove(board);
    } else {
      for (let i = 0; i < playerPossibleWins.length; i++) {
        const bestMove = playerPossibleWins[i].bestMove;
        if (typeof board[bestMove] === "number") {
          return bestMove;
        }
      }
    }
  } else {
    for (let i = 0; i < enemyPossibleWins.length; i++) {
      const bestMove = enemyPossibleWins[i].bestMove;
      if (typeof board[bestMove] === "number") {
        return bestMove;
      }
    }
  }
  return getRandomMove(board);
}
