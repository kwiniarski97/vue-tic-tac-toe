enum GameState{
    NOT_STARTED,
    IN_PROGRESS,
    WON, 
    TIED, 
    LOST
}

export default GameState;